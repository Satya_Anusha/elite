def isPerfect(n):
    sum1=0
    for i in range(1,n):
        if n % i == 0:
            sum1=sum1+i
    if sum1==n:
        return True
    else:
        return False
def getPerfectNumbers(m,n):
    a=[]
    if m<0 or n<0:
       return -1
    elif m>n:
        return -1
    else:
       for i in range(m,n):
        if isPerfect(i):
          a.append(i)
       return a
print(getPerfectNumbers(1,10))

