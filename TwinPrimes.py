def isPrime(num):
    count = 0
    for i in range(1,num):
        if num % i == 0:
            count = count + 1
    if count == 1:
        return True
    else:
        return False
def twinprimes(start,limit):
    a = []
    if start <= 0 or limit <= 0:
        return -1
    elif start > limit:
        return -2
    else:
        for j in range(start,limit):
            if isPrime(j+2) and isPrime(j):
                a.append((j,j+2))
         
        return a      
            
        
           
