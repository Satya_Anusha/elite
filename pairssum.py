a = [int(i) for i in input().split(',')]
num = int(input())
pairs = []

for i in a:
    if i not in pairs and num // 2 != i and (num - i) in a:
        pairs.extend([i, num - i])
        break

print(pairs) if pairs else print('')
