# cook your dish here
def is_prime(num):
    if num in [2,3,5,7]:
        return True
    if num % 2 == 0:
        return False
    r = 3
    while r * r <= num:
        if num % r == 0:
            return False
        r += 2
    return True
n = int(input())
l = []
for i in range(2,1000000000000):
 
   if is_prime(i):
     l.append(i)
   if len(l) == n:
     break
for i in range(len(l)):
    print(l[i])
