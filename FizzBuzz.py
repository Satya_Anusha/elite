def fizzBuzz(num):
        a = []
	num = int(num)
        if num > 0:
           for i in range(1,num + 1):
             if i % 15 == 0 :
                a.append("Fizz Buzz")
             elif i % 3 == 0 :
                a.append("Fizz")
             elif i % 5 == 0 :
                a.append("Buzz")
             else: 
                a.append(i)
	else:
		return "Invalid Inputs"
        return a 
   
