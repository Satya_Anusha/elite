def prime(n):
  if n in [2,3,5,7]:
    return True
  if n%2==0:
    return False
  r=3
  while r*r<=n:
    if n%r==0:
      return False
    r=r+2
    return True
prime(5)  
def check(n):
  p=[i for i in range(1,n) if prime(i)]
  for m in range(1,n):
    for i in range(len(p)):
      for  j in range(i,len(p)):
        if m==p[i]*p[j] and m+1==p[i]*p[j]:
          return (m,m+1)
print(check(20))   
