import numpy as np   

l = int(input("Enter the number of rows:")) 
n = (2*l - 1)
print("Enter the entries in a single line (separated by space): ") 
entry = str(input())
s = entry.replace('0', '1234')
entries = list(s.split())
matrix = np.array(entries).reshape(n, n)

def position_of_single_digit():
  pos = []
  for i in range(0,n,2):
    for j in range(0,n,2):
      if(len(matrix[i][j]) == 1):
        pos.append((matrix[i][j],i,j))
  return pos


def remove_single_digits():
  pos = position_of_single_digit()
  for i in range(0,len(pos)):
    x = pos[i][1]
    y = pos[i][2]
    for j in range(0,n,2):
      if(len(matrix[x][j]) != 1 or len(matrix[j][y]) != 1):
        matrix[x][j] = matrix[x][j].replace(pos[i][0],'')
        matrix[j][y] = matrix[j][y].replace(pos[i][0],'')
  return matrix

print(remove_single_digits())
  
def constraint():
  for i in range(0,n,2):
    for j in range(1,n,2):
      if(matrix[i][j] == '<'):
        matrix[i][j-1] = matrix[i][j-1].replace(str(l),'')
        matrix[i][j+1] = matrix[i][j+1].replace('1','')
      elif(matrix[i][j] == '>'):
        matrix[i][j-1] = matrix[i][j-1].replace('1','')
        matrix[i][j+1] = matrix[i][j+1].replace(str(l),'')
      elif(matrix[i][j] == '^'):
        matrix[i-1][j] = matrix[i-1][j].replace(str(l),'')
        matrix[i+1][j] = matrix[i+1][j].replace('1','')
      elif(matrix[i][j] == 'v'):
        matrix[i-1][j] = matrix[i-1][j].replace('1','')
        matrix[i+1][j] = matrix[i+1][j].replace(str(l),'')
    return matrix
  

print(constraint())

        
