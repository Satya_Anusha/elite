par_list = list(map(int,input().split()))
input_string = input("Enter elements separated by space:")
result_list = input_string.split()
sum1 = sum(par_list)
def invalid_case(scorelist):
   sum2 = sum(scorelist)
   if sum2 < 0:
       print("Enter valid input")
   else:
       result_game(par_list)
def result_game(result_list):
   total_list = []   
   my_dict = {"albatross":-3,"eagle":-2,"bride":-1,"par":0,"bogey":1,"double-bogey":2,"triple-bogey":3}
   for i in range(len(result_list)):
        p = i + my_dict[i]
        total_list.append(p)     
        invalid_case(total_list)
   return total_list
final_list=result_game(result_list)
sum2 = sum(final_list)
if sum2 - sum1 > 0:
  print("over par")
elif sum2 - sum1 < 0:
  print("under par")
else:
  print("on par")
