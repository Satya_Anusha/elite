x=int(input())
l = list(map(int,input().strip().split()))[:x]
def minSwaps(l):   
    n = len(l)
    arrpos = [*enumerate(l)] 
    arrpos.sort(key = lambda it:it[1]) 
    vis = {k:False for k in range(n)}
    ans = 0
    for i in range(n): 
        if vis[i] or arrpos[i][0] == i: 
            continue
        cycle_size = 0
        j = i 
        while not vis[j]: 
            vis[j] = True
            j = arrpos[j][0] 
            cycle_size += 1
        if cycle_size > 0: 
            ans += (cycle_size - 1) 
    return ans            
print(minSwaps(l))
