def divisors(n):
  l = [i for i in range(1,n+1) if n % i == 0]
  return l
def guess(P,R):
  possible_values = [j for j in range(1,(R // P) + 1) for d in divisors(j) if R == P * j + d]
  return (len(possible_values),possible_values)
x=int(input())
for m in range(x):
    P,R = map(int,input().split())
    print(guess(P,R))
    
