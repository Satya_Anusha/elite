import operator
def row_constraints_max(m):
  dic={}  
  for i in range(1,len(m),2):
    count=0
    for j in range(0,len(m[i]),2):
        if(m[i][j]=='^' or m[i][j]=='v'):
             count=count+1
    dic[i]=count
  key_max=max(dic.keys(),key=lambda k:dic)
  return max(zip(dic.values(),dic.keys()))
print(row_constraints_max([['.','>','.','.','.'],['^','.','^','<','.'],['.','.','.','<','.'],['.','.','.','.','^'],['.','.','.','<','.']]))


    
