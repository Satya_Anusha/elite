from itertools import permutations
x=int(input())
the_string = input()
string,query = the_string.split()
next_string=input()
L,R=next_string.split()
s1=string[L-1:R]
p=combination_string(s1)

def combination_string(p):
    comb = permutations(p) 
    s=[''.join(i) for i in list(comb)]
    return s
for i in range(len(p)):
   if(isPalindrome(p[i])):
      print(p[i])
           
def reverse(s): 
    return s[::-1] 
def isPalindrome(s): 
    rev = reverse(s) 
    if (s == rev): 
        return True
    return False
