from itertools import permutations as npr
def get_input():
  n=int(input())
  s,nqueries=input().split()
  queries=[input().split() for i in range(int(nqueries))]
  queries=[(int(q[0]),int(q[1])) for q in queries]
  return n,s,queries 
def gen_palindromes(s,queries):
  palindromes=[]
  for L,R in queries:
    palindromes.append(s[:L-1]+make_palindrome(s[L-1:R])+s[R:])
  return palindromes   
def is_palindrome(s):
  return s==s[::-1]
def make_palindrome(s):
  pals=[''.join(p) for p in set(npr(s)) if is_palindrome(p)]
  if (len(pals))==0:
    return s
  return sorted(pals)[0]  
n,s,queries=get_input()
palindromes=gen_palindromes(s,queries)
for i in palindromes:
  print(i)       

        


