import math
def prime(n):
  if n in [2,3,5,7]:
    return True
  if n % 2 == 0:
    return False
  r = 3
  while r * r <= n:
    if n % r == 0:
      return False
    r = r + 2
  return True
def prime_factors(n):
  l = []
  l = [i for i in range(n) if prime(i) and n % i == 0]
  return l
def sqrt_snooth(n):
  k = []
  z = []
  for i in range(2,n):
    z = prime_factors(i)
    if z[-1] < int(math.sqrt(i)):
      k.append(i)
  return len(k)
print(sqrt_snooth(100))
