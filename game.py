def setup():
    board=[["","",""],["","",""],["","",""]]
    return board
def make_move(board,who,where):
  i,j=where
  if board[i][j]=="":
    board[i][j]= who
  return board
def iswin(board):
  COLON=":"
  rows=[''.join(board[i] for i in range(3))]
  cols=[''.join(x) for x in [_for_ in zip(*rows)]]
  d1=''.join([board[i][i] for i in range(3)])           
  d2=''.join([board[i][2-i] for i in range(3)])
  check=rows+COLON+ cols + COLON + d1 +COLON +d2
  if "XXX" in check:
    return "X"
  if "OOO" in check:              
    return "O"  
  return ""  
b=setup()
b=make_move(b,"X",(0,0))
b=make_move(b,"O",(1,0))  
b=make_move(b,"X",(0,1))
b=make_move(b,"O",(2,1))

